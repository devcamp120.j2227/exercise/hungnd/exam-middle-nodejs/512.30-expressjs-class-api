// Import thư viện Express Js
const express = require("express");
// Khai báo thư viện mongo
const mongoose = require("mongoose");

// Khởi tạo 1 app express
const app = express();

// Khai báo 1 cổng 
const port = 8000;

// Khai báo để sử dụng bodyJson
app.use(express.json());

// Khai báo để sử dụng UTF-8
app.use(express.urlencoded({
    extended: true
}));

// Kết nối với mongo
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_User", (error) => {
    if (error) throw error;
    console.log('Successfully connected');
});

// Import userRouter và sử dụng
const { userRouter } = require("./app/routes/usersRouter");
app.use('/', userRouter);

// run app on declared port
app.listen(port, () => {
    console.log(`App running on port ${port}`);
});